# libnpce 正文抽取组件

**项目地址：**

- Github: https://github.com/tangyibo/libnpce

- Gitee: https://gitee.com/inrgihc/libnpce

## 一、组件介绍

### 1、功能介绍

 新闻文章正文抽取News Passage Content Extractor (NPCE)，是为抽取HTML中的文章正文而设计的。
 
 可用libnpce组件抽取新闻页面的如下字段信息：
 
 - （1）新闻标题
 
 - （2）发布时间
 
 - （3）来源及其URL链接地址
 
 - （4）正文文本内容
 
 - （5）正文图片信息（含图片在正文中的位置信息）
 
 - （6）正文中的文本链接等其他
 
### 2、集成方式

 该组件提供给予so动态链接库的调用接口、RESTful服务调用、python调用等接口：

 - （1）在python目录下提供对python调用的封装，请见：[python/example/example.py](python/example/example.py)；

 - （2）在server目录下提供RESTfull的接口的封装服务；

 - （3）支持常见的新闻APP新闻页的正文抽取；

 - （4）C/C++动态链接库调用请参见：https://tangyibo.github.io/libariry/2020/01/17/a-news-passage-content-extractor-library/

## 二、设计结构

### 1、程序结构

```
  \
   -include    头文件所在目录
   -src        源程序所在目录
   -test       测试和使用示例
   -cgifile    用于进行CGI部署的程序
   -parseapp   对APP正文抽取的组件封装
   -python     为python语言调用封装的通用接口
   -server     提供基于HTTP服务访问的RESTful接口
   -Makefile  
   -publish    发布版本
   -readme.txt 说明文件
   -bin        二进制文件生成目录
   -run.sh     运行测试程序脚本
```

### 2、环境准备

(1) 准备一台干净的Centos7操作系统;

(2) 使用如下命令安装gcc和c++环境:
```
yum install -y gcc gcc-c++
```

(3) 源码安装依赖

- 将C/C++的库封装为Python的接口，需要的条件是安装Python-devel,安装的步骤如下：
```
wget http://www.python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz
tar xJf Python-2.7.6.tar.xz
cd Python-2.7.6
./configure --enable-shared --prefix=/usr/local/python2.7
make
make install 
```
- 安装libiconv库

```
wget http://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.15.tar.gz
tar -zxvf libiconv-1.15.tar.gz
cd libiconv-1.15
./configure
make && make install

```

(4) 安装依赖
```
yum install -y epel-release openssl-devel zlib-devel zlib-devel c-ares-devel curl-devel python-pip
```

### 3、编译程序

```
git clone https://gitee.com/inrgihc/libnpce.git
cd libnpce/
make lib
make clean && make                 #编译生成libnpce库
make clean all -C parseapp         #编译生成libparseapp库
make clean all -C server        
make clean all -C python           #编译生成python可调用的libgolaxynpce库
cd /root/libnpce/python/server/
pip install -r requirements.txt
make clean && make build           #编译生成python的http接口服务器程序
```

其他编译选项详见Makefile文件

## 三、演示使用

### 1、安装部署

(1) 使用二进制包部署：
```
git clone https://gitee.com/inrgihc/libnpce.git
cd libnpce/publish/
tar zxvf httpd_npce_server_1.0.36_bin.tar.gz
cd httpd_npce_server/
./startup.sh
```

(2) 使用docker容器部署
```
docker run -d --name py_npce_httpd -p 7645:7645 inrgihc/py_npce_httpd:latest
```

### 2、服务调用

服务基于mongoose提供HTTP协议访问，参数提交以POST方式提交，详细参数设置如下：

```
 ----------------------------------------------------------------------
 |  参数名称     |     默认值      |     参数解释
 ----------------------------------------------------------------------
 |     url       |   不允许为空值  | 要抽取正文信息的URL(需URL编码)
 ----------------------------------------------------------------------
 |     img       |      0          | 是否抽取正文中的图片，1为是，0为否
 ----------------------------------------------------------------------
 |      flg      |      0          | 是否在正文中标记图片，1为是，0为否
 ----------------------------------------------------------------------
```
  注：图片在正文中的位置标记方式为"{IMG:N}",这里N为图片的序号。
  
  用CURL调用的示例如下所示：
  
```
$ curl "http://127.0.0.1:7654/npce" -d "img=1&flg=1&url=http://news.sina.com.cn/c/2016-11-07/doc-ifxxnffr6962826.shtml"
```

### 3 、演示截图

![picture](images/npce.png)

## 四、文档博客

(1) https://blog.csdn.net/inrgihc/article/details/103739874

(2) https://tangyibo.github.io/libariry/2020/01/17/a-news-passage-content-extractor-library/

## 五、问题反馈

如果您看到或使用了本工具，或您觉得本工具对您有价值，请为此项目**点个赞**，多谢！如果您在使用时遇到了bug，欢迎在issue中反馈。也可扫描下方二维码入群讨论：（加好友请注明："程序交流"）

![structure](images/weixin.PNG)
