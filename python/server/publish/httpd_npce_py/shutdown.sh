#!/bin/sh
if [ -f ".spid.txt" ]; then
	kill -9 $(cat .spid.txt)
	rm .spid.txt
fi

pid=$( pgrep httpd_npcepy)
kill -9 $pid

